I have monkey patched a method `another_flatten` in the Array class.
We could have gone with a refinement instead of a monkey patch.
But this way is a bit simpler and we are not affecting any existing
methods so it is fine to use it here.

To use the method require the `array` file and you would be able to
perform `array_instance.another_flatten` which should perform as
expected.

Look at the tests for example usages and results.

To run in terminal:

- install the required ruby version via rvm or rbenv

- run `bundle install`

- now you can run `rspec` to run all tests
