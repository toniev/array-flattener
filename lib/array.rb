require 'pry'

class Array
  def another_flatten
    flatten_array(self, [])
  end

  private

  def flatten_array array, result_array
    array.each do |item|
      if item.is_a?(Array)
        flatten_array(item, result_array)
      else
        result_array << item
      end
    end

    result_array
  end
end