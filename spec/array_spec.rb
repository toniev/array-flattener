require './lib/array'

describe Array do
  shared_examples 'a flattened array' do
    subject { array.another_flatten }

    it { is_expected.to eq flattened_array }
  end

  context 'when flattening a single level nested array' do
    let(:array) { [1, 'a', {b: 'c'}, [4, 5], 6] }
    let(:flattened_array) { [1, 'a', {b: 'c'}, 4, 5, 6] }

    it_behaves_like 'a flattened array'
  end

  context 'when flattening a deeper nested array' do
    let(:array) { [1, 'a', 3, [{b: 'c'}, [5, 6]]] }
    let(:flattened_array) { [1, 'a', 3, {b: 'c'}, 5, 6] }

    it_behaves_like 'a flattened array'
  end

  context 'when flattening an empty array' do
    let(:array) { [] }
    let(:flattened_array) { [] }

    it_behaves_like 'a flattened array'
  end

  context 'when flattening an array or nils' do
    let(:array) { [nil, [nil, [nil, nil], nil]] }
    let(:flattened_array) { [nil, nil, nil, nil, nil] }

    it_behaves_like 'a flattened array'
  end

  context 'when flattening an array with repeating elements' do
    let(:array) { [3, 4, [4, 5, [5, 6]], 7, 6, 5] }
    let(:flattened_array) { [3, 4, 4, 5, 5, 6, 7, 6, 5] }

    it_behaves_like 'a flattened array'
  end
end